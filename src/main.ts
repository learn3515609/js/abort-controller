const loadFetchButton = document.getElementById('loadFetch')!;
const loadPromiseButton = document.getElementById('loadPromise')!;
const cancelButton = document.getElementById('cancel')!;

let controller = new AbortController();

const fetchData = () => {
  setResult('loading data...');
  fetch(`https://jsonplaceholder.typicode.com/users/1?_delay=3000&t=${Date.now()}`, {
    method: 'GET',
    signal: controller.signal
  })
    .then(data => data.text())
    .then(setResult)
    .catch(setResult)
    .finally(() => controller = new AbortController());
}

const fetchPromiseData = () => {
  setResult('loading data...');
  new Promise((resolve: (data: string) => void, reject) => {
    controller.signal.addEventListener('abort', () => reject('cancel promise'))
    setTimeout(() => {
      fetch(`https://jsonplaceholder.typicode.com/users/1?t=${Date.now()}`)
        .then(data => data.text())
        .then(resolve)
    }, 3000)
  })
    .then(setResult)
    .catch(setResult)
    .finally(() => controller = new AbortController());
  
    
}

const cancelFetchData = () => {
  controller.abort();
}

const setResult = (data: string) => {
  const resultTextArea = document.getElementById('result') as HTMLTextAreaElement;
  resultTextArea && (resultTextArea.value = data);
}

loadFetchButton.addEventListener('click', fetchData);
loadPromiseButton.addEventListener('click', fetchPromiseData);
cancelButton.addEventListener('click', cancelFetchData);